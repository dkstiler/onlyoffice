#!/bin/bash

figlet "Mistborn: Nextcloud Credentials"

# generate onlyoffice .env files
ONLYOFFICE_PROD_FILE="./.envs/.production/.onlyoffice"
JWT_SECRET="$1"
echo "JWT_ENABLED=true" > $ONLYOFFICE_PROD_FILE
echo "JWT_SECRET=$JWT_SECRET" >> $ONLYOFFICE_PROD_FILE

